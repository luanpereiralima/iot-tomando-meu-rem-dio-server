var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var rotas      = require('./rotas');
var schedule = require('node-schedule');
var mqtt = require('mqtt');
var constantes = require('./constantes/constantes');

var client  = mqtt.connect('mqtt://'+constantes.IP);

client.subscribe("resposta");

var tick = require('./controller/time-tick');

var jTick = schedule.scheduleJob('10 * * * * *', function(){
  if(client.connected){
    tick.verificarHorarios(client);
  }else {
    console.log("MQTT não conectado para realizar as consultas");
  }
});

var jReconectMqtt = schedule.scheduleJob('*/30 * * * * *', function(){
  if(!client.connected && !client.reconnecting){
    console.log("Tentando reconectar MQTT");
    client = mqtt.connect('mqtt://'+constantes.IP);
  }
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || constantes.PORT;

app.use(rotas);

client.on('connect', function () {
  console.log("Conectado com sucesso no MQTT");
	client.subscribe('resposta');
});

client.on('message', function (topic, message) {
  console.log(message.toString());
});

app.listen(port);
console.log('Server rodando na porta ' + port);
