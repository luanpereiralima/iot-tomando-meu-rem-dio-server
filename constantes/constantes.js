function define(name, value) {
    Object.defineProperty(exports, name, {
        value: value,
        enumerable: true
    });
}

//define("IP", "52.67.171.60");
define("IP", "192.168.1.8");
define("PORT", 3005);
define("URL_BANCO", "52.67.171.60/remedio");
