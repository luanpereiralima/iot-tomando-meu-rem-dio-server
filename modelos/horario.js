var mongoose = require("../conexao/mongo");

var schema = mongoose.Schema;
var ObjectId = schema.ObjectId;

var HorarioSchema = schema({
  nomeRemedio: {type:String, required:true},
  space: {type: Number, required:true},
  descricao: String,
  listaHorarios: [Date]
}, { versionKey: false });

HorarioSchema.statics.buscar = function(retorno){
    this.find({})
    .exec(retorno);
};

module.exports = mongoose.model('Horario', HorarioSchema);
