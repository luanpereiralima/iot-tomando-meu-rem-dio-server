var Tick = (function(){

var Horario = require('../modelos/horario');

  return {
    verificarHorarios: function(client){
      Horario.buscar(function(err, horarios){
        if(err)
          return console.log(err);

        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();

        for(i=0; i < horarios.length; i++){
          var hor = horarios[i];
          console.log("server: "+d);
          for(j=0; j < hor.listaHorarios.length; j++){
            var horario = hor.listaHorarios[j];
            var dbd = new Date(horario);
            var hdb = dbd.getHours();
            var mdb = dbd.getMinutes();

            if(h === hdb && m === mdb){
              var objetoEnvio = {};
              objetoEnvio.nomeRemedio = hor.nomeRemedio;
              objetoEnvio.descricao = hor.descricao;
              objetoEnvio.horario = h+":"+m;
              objetoEnvio.space = hor.space;

              client.publish('android', JSON.stringify(objetoEnvio));
              client.publish('test', ""+hor.space);
            }

            console.log("BD: "+dbd);
          }

        }

      });
    }
  };
})();

module.exports = Tick;
