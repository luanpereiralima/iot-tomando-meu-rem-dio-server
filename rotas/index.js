var express = require('express');
var router = express.Router();

/*router.use(function(req, res, next){
  //FAZER A AUTENTICAÇÃO
  console.log('Autenticação!');
  console.log(req.headers['x-auth']);
  //res.send(401);
  next();
});*/

router.use('/horario', require('./horario'));

router.get('/', function(req, res) {
  res.json({ mensagem: 'Bem vindo!' });
});

module.exports = router;
