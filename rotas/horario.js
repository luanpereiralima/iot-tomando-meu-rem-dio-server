
var Horario = require('../modelos/horario');

var express = require('express');
var router = express.Router();

router.post('/', function(req, res){
  var horario = req.body.horario;

  if(!horario)
    return res.sendStatus(400);

  var json = JSON.parse(horario);

  Horario.update({space: json.space}, {$set: json}, {upsert: true}, function(err){

    if(err){
      console.log(err);
      return res.sendStatus(500);
    }
    return res.sendStatus(200);
  });

});

router.delete('/', function(req, res){
  var space = req.body.space;

  if(!space)
    return res.sendStatus(400);

  Horario.remove({space:space}, function(err){

    if(err){
      console.log(err);
      return res.sendStatus(500);
    }
    return res.sendStatus(200);
  });

});

router.get('/', function(req, res){

  Horario.buscar(
    function(err, posts){

    if(err){
      console.log(err);
      return res.sendStatus(500);
    }

    return res.json(posts);
  })

});

module.exports = router;
