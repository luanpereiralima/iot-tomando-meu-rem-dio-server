var mongoose = require('mongoose');
var constantes = require('../constantes/constantes');

mongoose.connect('mongodb://'+constantes.URL_BANCO, function(error){

  if(error)
    console.log("Erro na conexão com o Banco: "+error);
  else {
    console.log("Conectado no banco de dados!");
  }

});

mongoose.set('debug', true);

module.exports = mongoose;
